#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

// Context: You are given requirements for a utility function that needs to be written in C++. It has the following function signature:
//
//   uint64_t countNestedParentheses(const std::string& str);
//
// The requirements/behavior of the function can be described as:
//
//   Given an arbitrary string of characters, count the maximum level of nested parentheses in the string. Examples:
// 
//   0: ' '                                 // empty string, 0 levels of nesting
//   1: ( )                                 // max 1 level of nesting
//   1: ( ) )                               // max 1 level of nesting, unbalanced pair
//   2: ( 2 + ( 3 * 4 ) )                   // max 2 levels of nesting
//   3: ( ( ( 2 * 2 ) + 1 ) / 8 ) ( )       // max 3 levels of nesting
//
uint64_t countNestedParentheses(const std::string& str){
  uint64_t curNest = 0;
  uint64_t maxNest = 0;
  vector<uint64_t> results;
  for(auto &ch : str ) {
    if (ch == '('){
      curNest++;
      if (curNest > maxNest) maxNest = curNest;
      continue;
    }
    else if (ch == ')' && curNest > 0){
      curNest--;
    }
    if(curNest == 0 && maxNest > 0) {
      results.push_back(maxNest);
    }
  }
  //if all parentheses haven't been closed they don't count
  if (curNest != 0) maxNest = maxNest - curNest;
  results.push_back(maxNest);

  if(results.size() == 0) return 0;
  if(results.size() > 1) {
    sort(results.begin(), results.end());
  }
  return results[results.size() - 1];
}

void printBreakLine(){
  cout<<"--------------------------------------------------------------------"<<endl;
}

void runTest(int numTest, string str, uint64_t expectedResult) {
  printBreakLine();
  cout << "Test" << numTest << ":" << str;
  uint64_t result = countNestedParentheses(str);
  if (expectedResult == result) cout << ": Pass";
  else cout << ": Fail";
  cout << endl;
}

/*This program should be able to detect a maximum of 18446744073709551616 
nestedParentheses and is limited by the maximum size of uint64_t, although 
partically you may have memory issues loading in a string that large. */

/*Big O explanation: Program is 0(N * M log(M)), where N is the number of characters 
in the string and M is the number of nested parentheses sets. In most 
cases M should be much smaller than N but in the worst case M is N/2 */

int main(){
    cout<<"Welcome to NestedParentheses"<<endl;
    runTest(1, "( )", 1); //Test simple case
    runTest(2, ") )", 0); //Test case only 2 closing
    runTest(3, "( (", 0); //Test case only two openning
    runTest(4, "( ) )", 1); //More closing than oepnning
    runTest(5, "( 2 + ( 3 * 4 ) )", 2); //Testing other characters
    runTest(6, "( ( ( 2 * 2 ) + 1 ) / 8 ) ( )", 3); // Multiple nests and aditional characters
    runTest(7, ") ( ( ( ) ) )", 3); //closing start
    runTest(8, ") ( ( ( ) )", 2); //missing one closing on final nest
    runTest(9, ") ( ( ( ) ) ) ( ( ( ( ( ( ( ) )", 3); //false second nest that is potentially larger if the 3 closing brackets existed.
    runTest(10, ") ( ( ( ) ) ) ( ( ( ( ( ( ( ) ) ) ) )", 5); // same as above with closing brackets added
}